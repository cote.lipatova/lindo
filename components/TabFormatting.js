import React from 'react';
import { Text, View, Image, StyleSheet, Dimensions, TouchableOpacity, Platform, PermissionsAndroid, Button, AsyncStorage } from 'react-native';
import { Slider } from 'react-native';
import DeviceBrightness from 'react-native-device-brightness';
import convertDescriptorToString from 'jest-util/build/convertDescriptorToString';

export default class TabFormatting extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            backgroundColor: 'white',
            brightness: 0.2
        }
    }

    componentDidMount(){
        AsyncStorage.getItem('screenBrightness').then((brightness) => {
            this.setState({brightness: parseFloat(brightness) || this.state.brightness})
            console.log('Get item brightness:', brightness)
        })
    }
    
    getBrightnessLevel(){
        DeviceBrightness.getBrightnessLevel().then(brightness => {
            console.log('brightness', brightness)
        })
    }

    render() {
        const {
            onColorProfileChange, onFontSizeInc, onFontSizeDec,
            onFontFamilyChange
        } = this.props
        return (
            <View style={styles.container}>
                 {this.state.fontsList ? 
                    <View style={{position: 'absolute', height: '28%', width: '100%', backgroundColor: 'white', bottom: '10%'}}>
                        <FontsList
                         closeFontsList={() => this.closeFontsList()}/> 
                    </View> :  null 
                }
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={styles.cell}><Text>字体</Text></View>
                        <View style={styles.cell}>
                            <TouchableOpacity
                                onPress={() => onFontFamilyChange('monospace')}
                                style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{fontSize: 14}}>Bookery</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.cell}>
                            <TouchableOpacity
                                onPress={() => onFontFamilyChange('serif')}
                                style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={{fontSize: 14}}>Arial</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.cell}>
                            <TouchableOpacity
                                onPress={() => onFontFamilyChange('sans-serif-light')}
                                style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{fontSize: 14}}>Ceceil</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.cell}>
                            <TouchableOpacity
                                onPress={ this.props.openFontsList }>
                                <Image source={require('../icons/tab2-4.png')} style={{ width: 22 , height: 15 }}/>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row'}}>
                        <View style={styles.cell}><Text>字号</Text></View>
                        <View style={styles.cell}>
                            <TouchableOpacity
                                style={{ flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                                <Text style={{fontSize: 14}}>16号字</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.cell}>
                            <TouchableOpacity
                                onPress={() => onFontSizeInc(2)}
                                style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                <Image source={require('../icons/tab2-6.png')} style={{ width: 15, height: 15 }}/>                            
                            </TouchableOpacity>
                        </View>
                        <View style={styles.cell}>
                            <TouchableOpacity
                                onPress={() => onFontSizeDec(2)}
                                style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                <Image source={require('../icons/tab2-7.png')} style={{ width: 15, height: 15 }}/>                            
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row'}}>
                        <View style={styles.cell}><Text>主题</Text></View>
                        <View style={styles.cell}>
                            <TouchableOpacity
                                onPress={() => onColorProfileChange('dark')}
                                style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                <Image source={require('../icons/black.png')} style={{ width: 30, height: 10 }}/>                                                                                
                            </TouchableOpacity>
                        </View>
                        <View style={styles.cell}>
                            <TouchableOpacity
                                onPress={()=> onColorProfileChange('light')}
                                style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                <Image source={require('../icons/white.png')} style={{ width: 30, height: 10 }}/>                                                                                
                            </TouchableOpacity>
                        </View>
                        <View style={styles.cell}>
                            <TouchableOpacity
                                onPress={() => onColorProfileChange('green')}
                                style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                <Image source={require('../icons/green.png')} style={{ width: 30, height: 10 }}/>                                                                                
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row'}}>
                        <View style={styles.cell}><Text>亮度</Text></View>
                            <Slider
                                style={{width: 200, height: 40}}
                                minimumValue={0}
                                maximumValue={1}
                                step={0.1}
                                value={this.state.brightness}
                                onValueChange={(brightness) => {
                                    this.setState({ brightness });
                                    DeviceBrightness.setBrightnessLevel(brightness);
                                    AsyncStorage.setItem('screenBrightness', brightness.toString())
                                  }}
                                minimumTrackTintColor="#FDC900"
                                thumbTintColor="#FDC900"
                                maximumTrackTintColor="#ECECEC"
                            />
                    </View>
                </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        padding: 2
    },
    cell: {
        margin: 2,
        width: Dimensions.get('window').width / 5 -6,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
    },
})