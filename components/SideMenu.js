import React, { Component } from 'react'
import { ScrollView, Text, View, StyleSheet, TouchableOpacity } from 'react-native';
 
class SideMenu extends Component {
  constructor(props){
    super(props)
    this.state = {
      isOpened: false,
    }
  }

  componentWillUpdate(newProps, newState) {
    if (newProps.isOpened !== this.state.isOpened && newProps.isOpened !== newState.isOpened) {
      this.setState({isOpened: newProps.isOpened})
    }
  }

  onItemClick = (value) => {
    this.props.onItemClick(value)
    this.setState({ isOpened: false })
  }
 
  render() {
    const { items } = this.props  

    if (!this.state.isOpened) {
      return null
    }

    return (
      <View style={{ height: '100%', position: 'absolute', width: '80%', backgroundColor: 'white', zIndex: 2}}>
        <View style={{ flex: 1, flexDirection: 'column'}}>
          <View style={{flex: 1}}>
            <View style={{flex: 1, flexDirection: 'row', backgroundColor: '#F1F1F1'}}>
              <View style={styles.cell}>
                  <TouchableOpacity
                      style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                      <Text style={{fontSize: 14, marginTop: 5}}>目录</Text> 
                  </TouchableOpacity>
              </View>
              <View style={styles.cell}>
                  <TouchableOpacity
                      style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                      <Text style={{fontSize: 14, marginTop: 5}}>书签</Text>
                  </TouchableOpacity>
              </View>
              <View style={styles.cell}>
                  <TouchableOpacity
                      style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                      <Text style={{fontSize: 14, marginTop: 5}}>笔记</Text>
                  </TouchableOpacity>
              </View>
            </View>
          </View>
          <View style={{ flex: 7}}>
          {
                items.map(({content, value}) =>
            <View style={{flex: 1, flexDirection: 'column', width: 300, padding: 5}}>
              <TouchableOpacity onPress={ () => this.onItemClick(value) }>
                    <View style={{ height: 30, padding: 5}}>
                      <Text sytle={{ fontSize: 14, color: 'grey'}}>{content}</Text>
                    </View>
                    <View style={{ height: 30, padding: 5}}>
                      <Text style={{ fontSize: 16, color: 'black' }}>Description of the chapter</Text>
                    </View>
                    <View style={{ height: 30, padding: 5}}>
                      <Text style={{ fontSize: 14, color: 'grey'}}>第15页 
                        <Text style={{ marginLeft: 20}}>                2019.5.18 15:23</Text>
                      </Text>
                    </View>
              </TouchableOpacity>
            </View>
             )
            }
          </View>
        </View>
      </View>
    )
  }
}
 
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  cell: {
      flex: 1,
      padding: 2,
      height: 100,
      alignItems: 'center',
      justifyContent: 'center'
  }
});
 
export default SideMenu;