import React from 'react'
import { Text, View, StyleSheet, TouchableOpacity, Image, Dimensions } from 'react-native';


export default class ReaderHeader extends React.Component {
    render() {
        const {
            chapterName, progress
        } = this.props

        return (
                <View style={{ flex: 1, flexDirection: 'row', backgroundColor: '#F1F1F1'}}>
                    <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ fontSize: 14, color: 'grey' }}>{chapterName}</Text>
                    </View>
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <TouchableOpacity>
                            <Text style={{ fontSize: 12, color: 'black'}}>{progress}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
        )
    }
}