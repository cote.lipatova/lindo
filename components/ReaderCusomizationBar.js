import React from 'react'
import { Text, View, StyleSheet, TouchableOpacity, Image, Dimensions } from 'react-native';


import TabFormatting from './TabFormatting';
import TabSpasing from './TabSpasing';
import FontsList from '../components/FontsList';


export default class ReaderCustomizationBar extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            selectedTab: null,
        }
    }

    switchPopupContent = (selectedTab) =>{
        this.setState({
            selectedTab: this.state.selectedTab === selectedTab ? null : selectedTab
        })
    }

    render() {
        const {
            onColorProfileChange, onLineSpacingChange, onFontSizeInc, onFontSizeDec, 
            onFontFamilyChange, onSidebarOpen, openFontsList, onTextSpacingChange
        } = this.props

        const {
            selectedTab
        } = this.state

        return (
            <View style={{ flex: 1}}>

                { selectedTab && 
                 <View style={{ position: 'absolute', width: '100%', height: 200, bottom: 70, backgroundColor: 'white'}}>
                        <View style={{ flex: 1}}>
                        { selectedTab === 'formatting' ? 
                                <TabFormatting
                                    onColorProfileChange={onColorProfileChange}
                                    onFontFamilyChange={onFontFamilyChange}
                                    onFontSizeInc={onFontSizeInc} 
                                    onFontSizeDec={onFontSizeDec} 
                                    openFontsList={openFontsList}
                                /> : 
                          selectedTab === 'spacing' ? 
                                <TabSpasing
                                    onLineSpacingChange={onLineSpacingChange}
                                    onTextSpacingChange={onTextSpacingChange}
                                /> 
                          : null
                        }
                        </View>
                    </View>
                }

                <View style={{ position: 'absolute', width: '100%', height: 70, bottom: 0}}>
                    <View style={{ flex: 1, flexDirection: 'row'}}>
                        <View style={styles.tab}>
                            <TouchableOpacity onPress={onSidebarOpen}
                            style={styles.tab_content}>
                                <Image 
                                    source={require('../icons/tb1h.png')} 
                                    style={{height: 15, width: 15}}/>
                                <Text style={{fontSize: 10, marginTop: 5}}>
                                    目录
                                </Text> 
                            </TouchableOpacity>
                        </View>
                        <View style={styles.tab}>
                            <TouchableOpacity 
                                onPress={() => this.switchPopupContent('formatting')}
                                style={styles.tab_content}>
                                <Image 
                                    source={require('../icons/tb2h.png')} 
                                    style={{height: 15, width: 15}}/>
                                <Text style={{fontSize: 10, marginTop: 5}}>
                                    字体设置
                                </Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.tab}>
                            <TouchableOpacity 
                                onPress={() => this.switchPopupContent('spacing')}
                                style={styles.tab_content}>
                                <Image 
                                    source={require('../icons/tb3h.png')} 
                                    style={{height: 15, width: 15}}/>
                                <Text style={{fontSize: 10, marginTop: 5}}>
                                    间距
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </View> 
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        height: '100%',
        backgroundColor: 'white'
    },
    tab: {
        width: Dimensions.get('window').width / 3 -6,
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    tab_content: {
        flex: 1, 
        justifyContent: 'center', 
        alignItems: 'center'
    }
})