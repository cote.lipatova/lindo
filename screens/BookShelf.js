import React from 'react'
import {View, Text, StyleSheet, Image, AsyncStorage} from 'react-native'
import { withNavigationFocus } from 'react-navigation';

import BooksList from '../components/BooksList'
import LastBook from '../components/LastBook'

class BookShelf extends React.Component {

  componentDidUpdate(prevProps) {
    if (prevProps.isFocused !== this.props.isFocused) {
      console.log('Navigated')
      this.lastBookRef.current.update()
      this.booksListRef.current.update()
    }
  }

  lastBookRef = React.createRef()
  booksListRef = React.createRef()

  render() {
    return (
        <View style={styles.mainContainer}>
          <View 
            style={{ 
              position: 'absolute', 
              width: '100%', 
              height: '8%', 
              top: 0, 
              zIndex: 1, 
            }}
          >
            <View style={styles.toolbar}>
              <View style={{ flex: 1}}>
                <Image source={require('../images/b3.jpg')} style={styles.profile}/>
              </View>
              <View style={{ flex: 1}}>
                <Text>书架</Text>
              </View>
              <View style={{ flex: 1}}>
                <Text>上传图书</Text>
              </View>
            </View>
          </View>
          <View style={{ position: 'absolute', width: '100%', height: '40%', top: '8%', zIndex: 1}}>
            <View style={{flex: 1}}>
              <LastBook 
                ref={this.lastBookRef}
                navigation={this.props.navigation} 
              />
            </View>
          </View>
          <View style={{ position: 'absolute', zIndex: 1, width: '100%', height: '51%', bottom: 0}}>
              <BooksList 
                ref={this.booksListRef}
                navigation={this.props.navigation} 
              />
            </View>
          </View>
    );
  }
}

export default withNavigationFocus(BookShelf);

const styles = StyleSheet.create({
  mainContainer:{
    flex:1,
    flexDirection: 'column',
    backgroundColor: '#f8f8ff'
  },
  toolbar:{
    flex: 1,
    backgroundColor:'#fff',
    flexDirection:'row',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
 
  profile: {
    width: 30,
    height:30,
    borderRadius: 15,  
    },
  bookList: {
    marginTop: 10,
  }
})