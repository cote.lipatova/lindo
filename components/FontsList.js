import React from 'react';
import { Text, View, Image, StyleSheet, Dimensions, TouchableOpacity } from 'react-native';
import { CheckBox } from 'react-native-elements';

export default class FontsList extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            backgroundColor: 'white'
        }
    }
    
    render() {
        return (
            <View style={styles.container}>
                <View style={{ flex: 1, flexDirection: 'row', borderBottomColor: 'grey'}}>
                    <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center'}}>
                        <TouchableOpacity>
                            <Text>选择字体</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ flex:1, justifyContent: 'center', alignItems: 'center'}}>
                        <TouchableOpacity
                            style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <Image source={require('../icons/search_lq.png')} style={{ width: 15, height: 15 }}/>                                                                                
                        </TouchableOpacity>
                    </View>
                    <View style={{ flex:1, justifyContent: 'center', alignItems: 'center'}}>
                        <TouchableOpacity onPress={this.props.closeFontsList}>
                            <Text>取消</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                        <Text>完成</Text>
                    </View>
                </View>
                <View style={{ flex: 1, flexDirection: 'row'}}>
                    <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center'}}>
                        <TouchableOpacity>
                            <Text>字体名字与样式</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ flex:1, justifyContent: 'center', alignItems: 'center'}}>
                        <TouchableOpacity></TouchableOpacity>
                    </View>
                    <View style={{ flex:1, justifyContent: 'center', alignItems: 'center'}}>
                        <TouchableOpacity></TouchableOpacity>
                    </View>
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                        <CheckBox
                            checked={false}
                            uncheckedIcon="circle"
                        />
                    </View>
                </View>
                <View style={{ flex: 1, flexDirection: 'row' }}>
                    <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center'}}>
                        <TouchableOpacity>
                            <Text>字体名字与样式</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ flex:1, justifyContent: 'center', alignItems: 'center'}}>
                        <TouchableOpacity></TouchableOpacity>
                    </View>
                    <View style={{ flex:1, justifyContent: 'center', alignItems: 'center'}}>
                        <TouchableOpacity></TouchableOpacity>
                    </View>
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                        <CheckBox
                            checked={true}
                            checkedColor="#FDC900"
                            checkedIcon="check-circle"
                        />
                    </View>
                </View>
                <View style={{ flex: 1, flexDirection: 'row'}}>
                    <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center'}}>
                        <TouchableOpacity>
                            <Text>字体名字与样式</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ flex:1, justifyContent: 'center', alignItems: 'center'}}>
                        <TouchableOpacity></TouchableOpacity>
                    </View>
                    <View style={{ flex:1, justifyContent: 'center', alignItems: 'center'}}>
                        <TouchableOpacity></TouchableOpacity>
                    </View>
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                        <CheckBox
                            checked={false}
                            uncheckedIcon="circle"
                        />
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        height: '100%',
    },
    cell: {
        margin: 2,
        width: Dimensions.get('window').width / 4 -6,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center'
    }
})