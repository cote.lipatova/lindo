import React from 'react';
import { Text, View, StyleSheet, TouchableOpacity, Image, Dimensions, AsyncStorage } from 'react-native';

import SideMenu from '../components/SideMenu';
import DeviceBrightness from 'react-native-device-brightness';
import { withNavigationFocus } from 'react-navigation';

import ReaderHeader from '../components/ReaderHeader'
import ReaderCusomizationBar from '../components/ReaderCusomizationBar'
import Reader from '../components/Reader'
import books from '../data/books'

import colorProfiles from '../data/colorProfiles.json'

// import pdf from 'pdf-parse'

import RNFS from 'react-native-fs'

import {NativeModules} from 'react-native';
var PDFUtils = NativeModules.PDFUtils;


let bookPages = [[]]
let wordsOnLastPageLength = 0
let MAX_LENGTH = 400

class Book extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            backgroundColor: 'white',
            color: 'black',
            fontSize: 16,
            fontFamily: 'monospace',
            lineHeight: 30,
            wordwise: [],
            openWordWiseIdx: null,
            text: '',
            currentPage: 0,
            isSideBarOpen: false,
            pages: null,
            padding: 2,
        }
    }

    componentDidMount() {
        const bookID = this.props.navigation.getParam('bookID', undefined)
        if (bookID === undefined) {
            return alert('no book');
        }

        const book = books.find((book) => 
            book.bookID === bookID
        )

        const dir = '/storage/emulated/0/Android/data/com.lindo/files/'
        const filename = book.fileName

        console.log('BookId:', bookID)

        const txtFilepath = dir + filename + '.txt'
        const pdfFilepath = dir + filename + '.pdf'

        RNFS.exists(txtFilepath)
            .then(exists => {
                if (exists) {
                    console.log('Txt version exists')
                    RNFS.readFile(txtFilepath)
                        .then(content => {
                            console.log('Txt loaded')
                            console.log(content)
                            this.setState({ text: content })
                            this.splitString()
                        })
                } else {
                    console.log('Txt version does not exists')
                    PDFUtils.pdfToTxt(pdfFilepath, txtFilepath, () => {
                        RNFS.readFile(txtFilepath)
                            .then(content => {
                                this.setState({ text:content })
                                this.splitString()
                            })
                    })
                }
                AsyncStorage.setItem('lastRead', bookID.toString())  
                console.log('lastread:', bookID)
                AsyncStorage.getItem('screenBrightness').then((brightness) => {
                    if(brightness !== null)
                    {
                        DeviceBrightness.setBrightnessLevel(parseFloat(brightness));                    
                    }
                    console.log('brightness:', brightness)
                })
            })
    }

    splitString = () => {
        console.log('Splitting text to pages')
        console.log(this.state.text)

        const words_array = this.state.text.split(' ')
        this.setState({
            wordwise: words_array
        })
        
        let bookPages = [[]]

        words_array.forEach(word => {
            word = word.replace('\n', '')

            if (wordsOnLastPageLength + word.length > MAX_LENGTH) {
                bookPages.push([])
                wordsOnLastPageLength = 0
            }
            
            bookPages[bookPages.length - 1].push(word)
            wordsOnLastPageLength += word.length
        })

        console.log(`Splitted to ${bookPages.length} pages`)

        this.setState({pages: bookPages.slice(1, 20)})
    }

    onSidebarOpen = () => {
        console.log('Click')
        this.setState({isSideBarOpen: true})
    }
    
    onFontSizeInc = (value) => {
        this.setState({fontSize: this.state.fontSize + value});
    }

    onFontSizeDec = (value) => {
        this.setState({fontSize: this.state.fontSize - value});
    }
    
    onLineSpacingChange = (value) => {
        this.setState({lineHeight: value});
    }
    
    onFontFamilyChange = (value) => {
        this.setState({fontFamily: value});
    }

    onTextSpacingChange = (value) => {
        this.setState({padding: value})
    }

    onColorProfileChange = (profileName) => {
        profile = colorProfiles[profileName]

        if (!profile) {
            console.error('Unknown profile', profile)
        }

        this.setState({
            backgroundColor: profile.backgroundColor,
            color: profile.color
        })
        console.log(this.onColorProfileChange)
    }

    render() {
        const {
            backgroundColor, color, fontFamily, fontSize, lineHeight, currentPage, isSideBarOpen,
            pages, padding
        } = this.state

        const bookID = this.props.navigation.getParam('bookID', undefined)        

        return (
            <View style={styles.container}>
                <SideMenu 
                    isOpened={isSideBarOpen}
                    items={[
                        { content: 'Chapter 1', value: 1 },
                        { content: 'Chapter 2', value: 3 },
                        { content: 'Chapter 3', value: 5 },
                        { content: 'Chapter 4', value: 7 },
                        { content: 'Chapter 5', value: 10 },
                    ]}
                    onItemClick={(value) => this.setState({currentPage: value}) }
                />

                <View style={styles.header}>
                    <ReaderHeader 
                        chapterName="Chapter 1"
                        progress="39%"
                    />
                </View>

                <View style={styles.reader}>
                    <Reader 
                        bgColor={backgroundColor}
                        textColor={color}
                        fontSize={fontSize}
                        fontFamily={fontFamily}
                        lineHeight={lineHeight}
                        pages={pages}
                        currentPage={currentPage}
                        padding={padding}
                        bookID={bookID}
                    /> 
                </View>

                <View
                    style={styles.tabbar}>
                    <ReaderCusomizationBar 
                        onColorProfileChange={this.onColorProfileChange}
                        onLineSpacingChange={this.onLineSpacingChange}
                        onFontSizeInc={this.onFontSizeInc}
                        onFontSizeDec={this.onFontSizeDec}
                        onFontFamilyChange={this.onFontFamilyChange}
                        onSidebarOpen={this.onSidebarOpen}
                        onTextSpacingChange={this.onTextSpacingChange}
                    />
                </View>

            </View>
        );
    }
}

export default withNavigationFocus(Book);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        height: '100%',
        backgroundColor: 'white'
    },
    header: {
        position: 'absolute', 
        height: '10%', 
        width: '100%',  
        top:0
    },
    sideMenu: {
        position: 'absolute', 
        height: '100%', 
        width: '80%',
        top: 0,
        left: 0
    },
    reader: {
        position: 'absolute', 
        height: '70%', 
        width: '100%', 
        top: '10%' 
    },
    tabbar: {
        position: 'absolute', 
        height: '10%', 
        width: '100%',  
        bottom:0
    }

})