import React, { Component } from 'react';
import { Text, View, TouchableOpacity, StyleSheet, ScrollView, Image, ProgressBarAndroid, AsyncStorage } from 'react-native';
import * as Progress from 'react-native-progress';

import books from '../data/books';

export default class BooksList extends Component { 
   constructor(props){
      super(props)
      this.state = {
          readingProgress: null
      }
  }
   
   componentDidMount(){
      this.update()
   }

   update() {
      books.map((book) => {
         AsyncStorage.getItem('readingPercent' + book.bookID).then((readingPercentStr) => {
            console.log('progress:', readingPercentStr)
            this.setState({['readingProgress' + book.bookID]: parseFloat(readingPercentStr) || 0})
         })
      })
   }

   render() {
      console.log(books)
      return (
         <View style = {styles.container}>
            <ScrollView>
               <View>
                  <Text style = {styles.h2}>
                     我的书架 
                  </Text>
               </View>
               <View style={{ flex: 1, 
                              flexDirection: 'row', 
                              flexWrap: 'wrap', 
                              justifyContent: 'space-around' 
                           }}
               >
                  {
                     books.map((book) => (
                        <View>
                           <TouchableOpacity
                              key = {book.bookID}
                              style = {styles.card} 
                              onPress={() =>
                                 this.props.navigation.navigate('Book', {bookID: book.bookID})}
                           >
                              <Image  source={book.img} style = {styles.imageView}/>
                              <Text style = {styles.text}>
                                 {book.name}
                              </Text>
                              <Progress.Bar 
                                 marginTop={5} 
                                 progress={this.state['readingProgress' + book.bookID]} 
                                 width={100} 
                                 height={4} 
                                 color="#FDC900" 
                                 borderWidth={0} 
                                 unfilledColor="#F7F7F7" 
                              />             
                           </TouchableOpacity>
                        </View>
                     ))
                  }
               </View>
            </ScrollView>
         </View>
      )
   }
}

const styles = StyleSheet.create({
   container: {
      backgroundColor: '#fff',
      height: '100%'
    },

   card: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'flex-start'
   },
   h2: {
      color: '#000',
      marginLeft: 20,
      marginTop: 20,
      fontSize: 15,
   },
   text: {
      color: '#4f603c',
      marginTop: 5,
   },
   imageView:{
      width: 100,
      height:130,
      marginTop: 15,      
      },
   boxStyle: {
      height: 130, 
      width: 100,
      },
})