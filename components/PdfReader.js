
import React from 'react';
import { StyleSheet, Dimensions, View, TouchableOpacity, Image, Text } from 'react-native';

import Menu from './Menu';
import Tab2 from '../components/Tab2';
import Tab3 from '../components/Tab3';
import Pdf from 'react-native-pdf';

const { width } = Dimensions.get('window');

const style = {
  justifyContent: 'flex-start',
  alignItems: 'center', 
  width: width,
  height: '100%',
  flex: 1,
};

const filePath = {uri:'https://www.planetebook.com/free-ebooks/pride-and-prejudice.pdf'}
 
export default class PdfReader extends React.Component {
    constructor(props){
        super(props);
        this.state = {
          selectedTab: null
        }
    }

    switchPopupContent = (selectedTab) =>{
        if(this.state.selectedTab === selectedTab) {
            this.setState({selectedTab: null})
        } else {
            this.setState({selectedTab})
        }
    }

    componentDidMount() {
        RNFS.readFile( filePath, 'ascii')
        .then((res) => {
            const data = res.split("").map(x => x.charCodeAt(0));
            let uint8array = new TextEncoder("ascii"). encode(data);
            // console.log(uint8array);
        })
    }


    render() {
        const source = {uri:'https://www.planetebook.com/free-ebooks/pride-and-prejudice.pdf',cache:true};
        //const source = require('./test.pdf');  // ios only
        //const source = {uri:'bundle-assets://test.pdf'};
 
        //const source = {uri:'file:///sdcard/test.pdf'};
        //const source = {uri:"data:application/pdf;base64,..."};
 
        return (
            <View style={styles.container}>
                <View style={styles.cell_header}>
                    <Text style={{ fontSize: 14, color: 'grey' }}>Chapter 1 Pride and Prejudice </Text>
                </View>
                <View style={{ flex: 12}}>
                    <Pdf
                        style={{ flex: 1 }}
                        source={source}
                        horizontal={true}
                        onLoadComplete={(numberOfPages,filePath)=>{
                            console.log(`number of pages: ${numberOfPages}`);
                        }}
                        onPageChanged={(page,numberOfPages)=>{
                            console.log(`current page: ${page}`);
                        }}
                        onError={(error)=>{
                            console.log(error);
                        }}
                        style={styles.pdf}/>
                </View>
                { this.state.selectedTab && 
                    <View style={{ flex: 4}}>
                        {this.state.selectedTab === 'tab1' ? <Menu />: 
                        this.state.selectedTab === 'tab2' ? <Tab2 />:
                        this.state.selectedTab === 'tab3' ? <Tab3 />: null
                        }
                    </View> 
                }
            <View style={{ flex: 1}}>
                <View style={{ flex: 1, flexDirection: 'row' }}>
                    <View style={styles.cell}>
                        <TouchableOpacity onPress={() => this.switchPopupContent('tab1')}
                            style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                <Image source={require('../icons/tb1h.png')} style={styles.tab_icon}/>
                            <Text style={{fontSize: 10, marginTop: 5}}>目录</Text> 
                        </TouchableOpacity>
                    </View>
                    <View style={styles.cell}>
                        <TouchableOpacity onPress={() => this.switchPopupContent('tab2')}
                            style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <Image source={require('../icons/tb2h.png')} style={styles.tab_icon}/>
                            <Text style={{fontSize: 10, marginTop: 5}}>字体设置</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.cell}>
                        <TouchableOpacity onPress={() => this.switchPopupContent('tab3')}
                            style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <Image source={require('../icons/tb3h.png')} style={styles.tab_icon}/>
                            <Text style={{fontSize: 10, marginTop: 5}}>间距</Text>
                        </TouchableOpacity>
                    </View>
                </View>     
            </View>
        </View>
        )
  }
}
 
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    pdf: {
        flex:1,
        width:Dimensions.get('window').width,
    },
    cell: {
        margin: 2,
        width: Dimensions.get('window').width / 3 -6,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center'
    },
    cell_header: {
        margin: 2,
        width: Dimensions.get('window').width / 1 -6,
        height: 60,
        alignItems: 'center',
        justifyContent: 'center'
    },
    tab_icon: {
        width: 15,
        height: 15
    },
});