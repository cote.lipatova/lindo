export default [
    {
        "bookID": 1,
        "name":"飘",
        "img": require('../images/b1.jpg'),
        "fileName": "emma"
    },
    {   
        "bookID": 2,
        "name":"悲惨世界",
        "img": require('../images/b2.jpg'),        
        "fileName": "persuasion"
    },
    {   
        "bookID": 3,
        "name":"家、春、秋",
        "img": require('../images/b3.jpg'),        
        "fileName": "pride-and-prejudice"
    }
]