import React from 'react';
import {View, Text, StyleSheet, Image, TouchableOpacity, Dimensions, AsyncStorage, findNodeHandle} from 'react-native';
import * as Progress from 'react-native-progress';

import books from '../data/books';

export default class LastBook extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            bookID: null,
            readingProgress: null
        }
    }

    componentDidMount(){
        this.update()
    }

    update() {
        console.log('update')
        AsyncStorage.getItem('lastRead').then((bookID) => {
            this.setState({bookID: parseInt(bookID)})
        })
        AsyncStorage.getItem('readingPercent').then((readingPercentStr) => {
            this.setState({readingProgress: parseFloat(readingPercentStr) || 0})
        })
    }

  render() {
    const book = books.find((book) => book.bookID === this.state.bookID)

    if (book === undefined) {
        return null
    }
 
    return (
        <View style = {styles.container}>
            <View style={{ flex: 1, flexDirection: 'row', marginTop: 30}}>
                <View style={styles.card1}>
                    <Image resizeMode="contain" source={book.img} style={styles.imageView}/>
                </View>
                <View style={styles.card2}>
                    <Text style = {{ fontSize: 20, fontWeight: '400', marginTop: 15}}>
                        {book.name}
                    </Text>
                    <Text style = {{ fontSize: 14, color: 'grey', marginTop: 10}}>
                        阅读了
                        <Text style={{ color: '#FDC900'}}>32%</Text>
                    </Text> 
                    <Progress.Bar 
                        marginTop={5} 
                        progress={this.state.readingProgress} 
                        width={150} 
                        color="#FDC900" 
                        borderWidth={0} 
                        unfilledColor="#F7F7F7" />
                    <TouchableOpacity 
                        style={styles.button}
                        onPress={() =>
                        this.props.navigation.navigate('Book', {bookID: book.bookID})}>
                        <Text>继续阅读</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    );
  }
}

const styles = StyleSheet.create({
    container:{
        backgroundColor:'#fff',
        height: '100%',
        paddingBottom: 30
    },
    card1: {
        margin: 2,
        width: Dimensions.get('window').width / 2 -6,
        height: 200,
        justifyContent: 'center',
        alignItems: 'center',
    },
    card2: {
        margin: 2,
        width: Dimensions.get('window').width / 2 -6,
        height: 200,
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
    },
    imageView:{
        height: '100%'
    },
    title:{
        color:'#000',
        fontWeight: 'bold',
        fontSize: 20,
    },
    button: {
        backgroundColor: '#FDC900',
        height: 35,
        width: '70%',
        borderRadius: 30,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 72
    },
});