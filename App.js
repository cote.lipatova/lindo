import React from 'react';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import BookShelf from './screens/BookShelf';
import Book from './screens/Book';

const AppNavigator = createStackNavigator({
  BookShelf: { 
    screen: BookShelf, 
    navigationOptions: { header: null }
  },
  Book: { 
    screen: Book, 
    navigationOptions: { header: null }
  },
});

const App = createAppContainer(AppNavigator)

export default App;
