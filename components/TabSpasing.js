import React from 'react';
import { Text, View, Image, StyleSheet, Dimensions, TouchableOpacity } from 'react-native';

export default class TabSpasing extends React.Component {
    constructor(props){
        super(props)
        this.state = {
        }
    }
    render() {
        const {
            onLineSpacingChange, onTextSpacingChange
        } = this.props
        return (
            <View style={{ flex: 1,
                            flexDirection: 'column',
                            padding: 2}}>
                <View style={{ flex: 1, flexDirection: 'row'}}>
                    <View style={styles.cell}><Text>行间距</Text></View>
                    <View style={{...styles.cell}}>
                        <TouchableOpacity
                            onPress={() => onLineSpacingChange(20)}
                            >
                            <Image source={require('../icons/tab1-1.png')} style={{ width: 15, height: 15 }}/>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.cell}>
                        <TouchableOpacity
                            onPress={() => onLineSpacingChange(30)}
                            >
                            <Image source={require('../icons/tab1-2.png')} style={{ width: 15, height: 15 }}/>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.cell}>
                        <TouchableOpacity
                            onPress={() => onLineSpacingChange(40)}
                            >
                            <Image source={require('../icons/tab1-3.png')} style={{ width: 15, height: 15 }}/>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{ flex: 1, flexDirection: 'row'}}>
                    <View style={styles.cell}><Text>行间距</Text></View>
                    <View style={styles.cell}>
                        <TouchableOpacity
                            onPress={() => onTextSpacingChange(1)}
                            >
                            <Image source={require('../icons/tab1-4.png')} style={{ width: 15, height: 15 }}/>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.cell}>
                        <TouchableOpacity
                            onPress={() => onTextSpacingChange(3)}
                            >
                            <Image source={require('../icons/tab1-5.png')} style={{ width: 15, height: 15 }}/>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.cell}>
                        <TouchableOpacity
                            onPress={() => onTextSpacingChange(5)}
                            >
                            <Image source={require('../icons/tab1-6.png')} style={{ width: 15, height: 15 }}/>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{ flex: 1, flexDirection: 'row'}}>
                    <View style={styles.cell}><Text>方向</Text></View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        padding: 2
    },
    cell: {
        margin: 2,
        width: Dimensions.get('window').width / 4 -6,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center'
    }
})