import React from 'react'
import { Pages } from 'react-native-pages';
import { Text, View, StyleSheet, TouchableOpacity, Image, Dimensions, AsyncStorage } from 'react-native';

const word_wise = {
    5: "wordwise5",
    7: "wordwise7",
    8: "wordwise8",
    9: "wordwise9",
    11: "wordwise_11",
    13: "wordwise_13",
    19: "wordwise_19",
    20: "wordwise_20",
    23: "wordwise_23",
    25: "wordwise_25",
    28: "wordwise_28",
    32: "wordwise_32",
    35: "wordwise_35",
    40: "wordwise_40",
    41: "wordwise_41",
    43: "wordwise_43",
    47: "wordwise_47",
    52: "wordwise_52",
}

class Page extends React.Component {
    
    componentDidMount() {
        /*
            !!!IMPORTANT!!!
                On the next few lines you could see awful and wierd solution for 
                watching the reading progress (pages scrolling).
            
                Reason: react-native-pages does not provide any callback on page scrolling
            
                Solution: periodicaly check of _value property of the progress animation provided by
                react-native-pages. This watch apply only on the last page (done by passing exta prop 
                "watchOnThisPage" into this component)
        */
        
        /* Start of the strange code */
        if (this.props.watchOnThisPage) {
            const watchProgress = (prevPage) => () => {
                const currentPage = Math.round(this.props.progress._a._value)
                const totalPages = -1 * this.props.progress._b._value + 1

                if (currentPage !== prevPage) {
                    console.log('Scrolled on the new page:', currentPage, totalPages)

                    const readingPercent = currentPage/totalPages
                    AsyncStorage.setItem('readingPercent' + this.props.bookID, readingPercent.toString())
                }

                setTimeout(watchProgress(currentPage), 1000)
            }
            watchProgress(null)()
        }
        /* End of the strange code */
    }
    render() {
        return this.props.children
    }
}

export default class Reader extends React.Component {
    constructor(props){
        super(props)

        this.state = {
            openWordWiseIdx: null,
        }
    }

    openWordWise = (idx) => {
        this.setState({openWordWiseIdx: idx})
    }

    closeWordWise = () => {
        this.setState({openWordWiseIdx: null})
    }

    componentWillUpdate(nextProps) {
        if (this.pagesRef.current) {
            this.pagesRef.current.scrollToPage(nextProps.currentPage)
        }
    }

    pagesRef = React.createRef()

    render() {
        console.log(this.state.openWordWiseIdx)
        const {
            bgColor, lineHeight, fontFamily, fontSize, textColor, pages,
            currentPage, padding, bookID
        } = this.props

        const { openWordWiseIdx } = this.state

        if (pages === null) {
            return (
                <View>
                    <Text>Loading...</Text>
                </View>
            )
        }

        return (
            <View 
                style={{ 
                    flex: 1
                }}
            >
                <Pages 
                    indicatorPosition="none" 
                    ref={this.pagesRef} 
                    startPage={currentPage}
                >
                    { pages.map((page, key) => 
                        <Page 
                            watchOnThisPage={key === pages.length - 1}
                            bookID={bookID}
                        >
                            <View 
                                style={{ 
                                    flex: 1, 
                                    flexDirection: 'row', 
                                    flexWrap: 'wrap' ,
                                    backgroundColor: bgColor
                                }}
                            >
                                { 
                                    page.map((word, idx) => 
                                    <TouchableOpacity 
                                    style={{ padding: padding}}
                                    onPress={()=> this.openWordWise(idx)}>
                                            {
                                                ( idx === openWordWiseIdx) && 
                                                    <TouchableOpacity style={{ position: 'absolute', width: 100, bottom: 26}}>
                                                        <Text> {word_wise[idx]} </Text>
                                                        <Image source={require('../icons/ww.png')} style={{ width: 50 , height: 5 }}/>
                                                    </TouchableOpacity>
                                            }
                                                <Text 
                                                    style={{
                                                        lineHeight: lineHeight, 
                                                        fontFamily: fontFamily, 
                                                        fontSize: fontSize, 
                                                        color: textColor
                                                    }}
                                                >
                                                    {word}
                                                </Text>
                                        </TouchableOpacity>
                                    )
                                }
                            </View>
                        </Page>
                    )}
                </Pages>
            </View>
        )
    }
}