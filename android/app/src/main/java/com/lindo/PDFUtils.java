package com.lindo;
 
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.uimanager.IllegalViewOperationException;
import com.facebook.react.bridge.WritableNativeMap;
import com.facebook.react.bridge.WritableMap;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;

import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;
 
public class PDFUtils extends ReactContextBaseJavaModule {
 
    public PDFUtils(ReactApplicationContext reactContext) {
        super(reactContext); //required by React Native
    }
 
    @Override
    //getName is required to define the name of the module represented in JavaScript
    public String getName() { 
        return "PDFUtils";
    }
 
    @ReactMethod
    public void getDocumentPagesCount(String filepath, Callback onDone) {
        try {
            PdfReader pdfReader = new PdfReader(filepath);
            int pages = pdfReader.getNumberOfPages(); 

            onDone.invoke(pages);

            pdfReader.close();
        } catch(IOException e) {

        }
    }

    @ReactMethod
    public void pdfToTxt(String inFilepath, String outFilepath, Callback onDone) {
        try {
            FileWriter fw = new FileWriter(outFilepath);
            
            PdfReader pdfReader = new PdfReader(inFilepath);
            int pages = pdfReader.getNumberOfPages();
            
            for (int i = 1; i <= pages; i++) { 
                try {
                    String pageContent = PdfTextExtractor.getTextFromPage(pdfReader, i);
                    fw.write(pageContent + "\n");
                } catch(Exception e) {

                }
            }
    
            fw.close();
            pdfReader.close();

            onDone.invoke();
        } catch(IOException e) {

        }
    }
}